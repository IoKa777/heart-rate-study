---
title: "Sleep ALL - correlations"
author: "Ioanna"
date: "7 juni 2016"
output: pdf_document
---

```{r echo=FALSE, message=FALSE, warning=FALSE}

library(ggplot2)
library(plyr)
library(moments)
library(gridExtra)
library(car)
library(reshape)
testTransformations<- function(data, colnum){
  data[,colnum]<- as.numeric(data[,colnum])
  data$a<- log10(data[,colnum]+104)
  print("LOG")
  ares<-data.frame(W=shapiro.test(a)$statistic, p.value=shapiro.test(a)$p.value, skewness=skewness(a), kurtosis=kurtosis(a))
  print(ares)
  
  data$b<- sqrt(data[,colnum]+104)
  print("SQRT")
  bres<-data.frame(W=shapiro.test(b)$statistic, p.value=shapiro.test(b)$p.value, skewness=skewness(b), kurtosis=kurtosis(b))
  print(bres)
  
  data$c<- 1/sqrt(data[,colnum]+104)
  print("1/SQRT")
  cres<-data.frame(W=shapiro.test(c)$statistic, p.value=shapiro.test(c)$p.value, skewness=skewness(c), kurtosis=kurtosis(c))
  print(cres)
  
  data$d<-1/data[,colnum]
  print("Reciprocal")
  dres<-data.frame(W=shapiro.test(d)$statistic, p.value=shapiro.test(d)$p.value, skewness=skewness(d), kurtosis=kurtosis(d))
  print(dres)
  
  data$e<- data[,colnum]^2
  print("square")
  eres<-data.frame(W=shapiro.test(e)$statistic, p.value=shapiro.test(e)$p.value, skewness=skewness(e), kurtosis=kurtosis(e))
  print(eres)
  
  data$f<- data[,colnum]^3
  print("cube")
  fres<-data.frame(W=shapiro.test(f)$statistic, p.value=shapiro.test(f)$p.value, skewness=skewness(f), kurtosis=kurtosis(f))
  print(fres)
  
  data$g<- log(data[,colnum]+sqrt(data[,colnum]^2+1))
  gres<-data.frame(W=shapiro.test(g)$statistic, p.value=shapiro.test(g)$p.value, skewness=skewness(g), kurtosis=kurtosis(g))
  print(gres)
  
  return(data)
}

#get Significance code for p-value of statistical test
getSigCode<- function(statistic){
  sig<-ifelse(statistic<0.001, "\u002A\u002A\u002A", ifelse(statistic<0.01, "\u002A\u002A", ifelse(statistic<0.05,"\u002A",ifelse(statistic<0.1,"\u2022",""))))
  return(sig)
}

# Get upper triangle of the correlation matrix
get_upper_tri <- function(cormat){
  cormat[lower.tri(cormat)]<- NA
  return(cormat)
}

# Get lower triangle of the correlation matrix
get_lower_tri <- function(cormat){
  cormat[upper.tri(cormat)]<- NA
  return(cormat)
}

setwd("C:\\Users\\310199005\\Documents\\Philips_HR_study_WeeklyQuestionnaires_P+B\\R")
sleepALL<- read.table("sleepALL08062015.wide.csv", header = T, sep = ",")
mbi<- read.table("MBIscore_wide_22022016.csv", header = T, sep = ",")
mbi$grp<-NULL
pss<- read.table("PSSscore_wide_22022016.csv", header = T, sep = ",")
pss$grp<-NULL
wemwbs<-read.table("WEMWBSscore_wide_22022016.csv", header = T, sep = ",")
wemwbs$grp<-NULL
psqi<- read.table("PSQIscore_wide_22022016.csv", header = T, sep = ",")
psqi$grp<-NULL


#exclude B22 and its match // merge dataframes
idxB22<- which(sleepALL$pp=="B22" | sleepALL$pp=="P49")
sleepALL<- sleepALL[-idxB22,]
df1<- merge(sleepALL, mbi, by=c("pp"), all.x = TRUE)
df2<- merge(df1, pss, by=c("pp"), all.x = TRUE)
df3<- merge(df2, wemwbs, by=c("pp"), all.x = TRUE)
dfALL<- merge(df3, psqi, by=c("pp"), all.x = TRUE)

#Physiological measures differences

# dfALL$DIBI<- dfALL$meanIBI.2 - dfALL$meanIBI.1
# dfALL$Dlg.IBI<- log(dfALL$meanIBI.2) - log(dfALL$meanIBI.1)
dfALL$Dsq.SDNN<- dfALL$sq.SDNN.2- dfALL$sq.SDNN.1
# dfALL$DSDNN<- dfALL$SDNN.2 - dfALL$SDNN.1
# dfALL$DRMSSD<- dfALL$RMSSD.2 - dfALL$RMSSD.1
dfALL$Dsq.RMSSD<- dfALL$sq.RMSSD.2 - dfALL$sq.RMSSD.1
# dfALL$Drec.HRtot<- dfALL$rec.meanHRtot.2 - dfALL$rec.meanHRtot.1
# dfALL$Drec.HRinv<- dfALL$rec.meanHRinv.2 - dfALL$rec.meanHRinv.1
dfALL$DHRtot<- dfALL$mdnHRtot.2 - dfALL$mdnHRtot.1
# dfALL$Dlg.HRtot<- log(dfALL$meanHRtot.2) - log(dfALL$meanHRtot.1)
# dfALL$DHRinv<- dfALL$meanHRinv.2 - dfALL$meanHRinv.1
# dfALL$Dlg.HRinv<- log(dfALL$meanHRinv.2) - log(dfALL$meanHRinv.1)
# #Differences in subjective measures
# dfALL$lg.PE1<- log(dfALL$meanPE.1+1)
# dfALL$lg.PE2<- log(dfALL$meanPE.2+1)
# 
# dfALL$lg.EX1<- log(dfALL$meanEX.1)
# dfALL$lg.EX2<- log(dfALL$meanEX.2)
# 
# dfALL$lg.CY1<- log(dfALL$meanCY.1+1)
# dfALL$lg.CY2<- log(dfALL$meanCY.2+1)
# 
# dfALL$Dlg.PE<- dfALL$lg.PE2 - dfALL$lg.PE1
# dfALL$Dlg.EX<- dfALL$lg.EX2 - dfALL$lg.EX1
# dfALL$Dlg.CY<- dfALL$lg.CY2 - dfALL$lg.CY1

dfALL$DPE<- dfALL$meanPE.2 - dfALL$meanPE.1
dfALL$DEX<- dfALL$meanEX.2 - dfALL$meanEX.1
dfALL$DCY<- dfALL$meanCY.2 - dfALL$meanCY.1

dfALL$DPSSnew<- dfALL$PSSnew.2 - dfALL$PSSnew.1
dfALL$DWEMWBSnew<- dfALL$WEMWBSnew.2 - dfALL$WEMWBSnew.1
dfALL$Dlg.PSQI<- log(dfALL$PSQI.2+1) - (dfALL$PSQI.1+1)

cat("Normality tests for subjective measures")
sh.DPE<-shapiro.test(dfALL$DPE)
cat(sh.DPE$data.name, "\n")
print(paste("W:", round(sh.DPE$statistic,3), ",   p-value:", round(sh.DPE$p.value, 4)))
sh.DEX<-shapiro.test(dfALL$DEX)
cat(sh.DEX$data.name, "\n")
print(paste("W:", round(sh.DEX$statistic,3), ",   p-value:", round(sh.DEX$p.value, 4)))
sh.DCY<-shapiro.test(dfALL$DCY)
cat(sh.DCY$data.name, "\n")
print(paste("W:", round(sh.DCY$statistic,3), ",   p-value:", round(sh.DCY$p.value, 4)))
sh.DPSS<-shapiro.test(dfALL$DPSSnew)
cat(sh.DPSS$data.name, "\n")
print(paste("W:", round(sh.DPSS$statistic,3), ",   p-value:", round(sh.DPSS$p.value, 4)))
sh.WEMWBS<-shapiro.test(dfALL$DWEMWBSnew)
cat(sh.WEMWBS$data.name, "\n")
print(paste("W:", round(sh.WEMWBS$statistic,3), ",   p-value:", round(sh.WEMWBS$p.value, 4)))
sh.PSQI<-shapiro.test(dfALL$Dlg.PSQI)
cat(sh.PSQI$data.name, "\n")
print(paste("W:", round(sh.PSQI$statistic,3), ",   p-value:", round(sh.PSQI$p.value, 4)))

cat("Normality tests for physiological measures")
# sh.DIBI<- shapiro.test(dfALL$DIBI)
# cat(sh.DIBI$data.name, "\n")
# print(paste("W:", round(sh.DIBI$statistic,3), ",   p-value:", round(sh.DIBI$p.value, 4)))
sh.DSDNN<- shapiro.test(dfALL$Dsq.SDNN)
cat(sh.DSDNN$data.name, "\n")
print(paste("W:", round(sh.DSDNN$statistic,3), ",   p-value:", round(sh.DSDNN$p.value, 4)))
sh.DRMSSD<- shapiro.test(dfALL$Dsq.RMSSD)
cat(sh.DRMSSD$data.name, "\n")
print(paste("W:", round(sh.DRMSSD$statistic,3), ",   p-value:", round(sh.DRMSSD$p.value, 4)))
sh.DHRtot<- shapiro.test(dfALL$DHRtot)
cat(sh.DHRtot$data.name, "\n")
print(paste("W:", round(sh.DHRtot$statistic,3), ",   p-value:", round(sh.DHRtot$p.value, 4)))
# sh.DHRinv<- shapiro.test(dfALL$DHRinv)
# cat(sh.DHRinv$data.name, "\n")
# print(paste("W:", round(sh.DHRinv$statistic,3), ",   p-value:", round(sh.DHRinv$p.value, 4)))


```

The normality tests presented above showed that $DIBI$, $DHRtot$ and $DHRinv$ are not normally distributed. Before preceeding, we plotted the data for these differences for inspection. 



```{r echo=FALSE, warning=FALSE, message=FALSE,}
hDIBI<-ggplot(dfALL, aes(x=DIBI, fill=grp)) +
  geom_histogram(binwidth=15, alpha=.5, position="identity")+
  theme(legend.position="top")
hHRtot<-ggplot(dfALL, aes(x=DHRtot, fill=grp)) +
  geom_histogram(binwidth=2, alpha=.5, position="identity")+
  theme(legend.position="top")
hHRinv<-ggplot(dfALL, aes(x=DHRinv, fill=grp)) +
  geom_histogram(binwidth=2, alpha=.5, position="identity")+
  theme(legend.position="top")

grid.arrange(hDIBI, hHRtot, hHRinv,nrow=2)


# pairs(dfALL[, c(37:41, 48:53)], col=dfALL$grp)

# dfALL[, c(37:41, 48:53)]

library(ggplot2)
library(Hmisc)

idx_out<- which(dfALL$pp %in% c( "B02",  "P16"))
dfALL<- dfALL[-idx_out,]

mydf<- dfALL[, c(35, 33:34, 36:41)]



cormat<- round(rcorr(as.matrix(mydf))$r, 2)
pmat<- round(rcorr(as.matrix(mydf))$P, 4)

# flattenCorrMatrix
# cormat1 : matrix of the correlation coefficients
# pmat1 : matrix of the correlation p-values
flattenCorrMatrix <- function(cormat, pmat) {
  ut <- upper.tri(cormat, diag = TRUE)
  data.frame(
    row = rownames(cormat)[row(cormat)[ut]],
    column = rownames(cormat)[col(cormat)[ut]],
    cor  =(cormat)[ut],
    p = pmat[ut]
  )
}

flat_dat<-flattenCorrMatrix(cormat, pmat)
adj.p<- 0.05/6
flat_dat$p.sym<- ifelse(flat_dat$p>=adj.p, "", ifelse(flat_dat$p>=0.001,"\u002A", "\u002A\u002A" ))

# melted_cormat <- melt(get_lower_tri(cormat))
# # melted_cormat <- melt(cormat)
# names(melted_cormat)[3]<- "cor"
# melted_pmat<- melt(get_lower_tri(pmat))
# names(melted_pmat)[3]<- "p.value"
# 
# flat_dat<- data.frame(melted_cormat, p.value=melted_pmat$p.value)
# flat_dat$p.sym<- getSigCode(flat_dat$p.value)
# 
# flat_dat$Var1<- factor(flat_dat$Var1, ordered = TRUE)
# flat_dat$Var2<- factor(flat_dat$Var2, ordered = TRUE)
# flat_dat<- na.omit(flat_dat, cols="cor")

idx1<- which(flat_dat$row %in% c("DPE", "DCY", "DEX", "DPSSnew", "DWEMWBSnew", "Dlg.PSQI"))
idx2<- which(flat_dat$column %in% c("Dsq.SDNN", "Dsq.RMSSD", "DHRtot"))

flat_less<- flat_dat[-c(idx1, idx2),]
flat_less$row<- factor(flat_less$row, levels = c("DHRtot", "Dsq.SDNN", "Dsq.RMSSD"), ordered = T)
flat_less$column<- factor(flat_less$column, levels = c("DPE", "DEX", "DCY", "DPSSnew", "DWEMWBSnew", "Dlg.PSQI"), ordered = T)

free_less<-ggplot(flat_less, aes(column, row, fill = cor))+
  geom_tile(color = "white")+
  scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
                       midpoint = 0, limit = c(-1,1), space = "Lab", 
                       name="Pearson\nCorrelation") +
  geom_text(aes(label = sprintf("%1.2f",cor)), color = "black", size = 3.5, vjust = 0) + 
  geom_text(aes(label = p.sym), color = "black", size = 3.5, vjust = 1.5) +
  theme_minimal()+ # minimal theme
  # theme(axis.text.x = element_text(angle = 45, vjust = 1, 
  #                                  size = 12, hjust = 1))+
  theme(axis.text.x=element_text(angle=45, vjust = 1, hjust=1, size=10),
         axis.text.y=element_text(size=10),
         axis.ticks=element_blank(),
         panel.border=element_blank(),
         panel.background=element_blank(),
         legend.position="top",
         legend.text=element_text(size=10),
         legend.title=element_text(size=10))+
  coord_fixed()+
  # scale_x_discrete(labels=c(expression(paste(Delta, "EX")), expression(paste(bold(Delta), "CY")), expression(paste(bold(Delta), "PSS")), expression(paste(bold(Delta), "WEMWBS")), expression(paste(bold(Delta), "lg.PSQi"))))+
  # scale_y_discrete(labels=c(expression(paste(bold(Delta), "PE")),expression(paste(bold(Delta), "EX")), expression(paste(bold(Delta), "CY")), expression(paste(bold(Delta), "PSS")), expression(paste(bold(Delta), "WEMWBS"))))+
  
  ylab("Cardiovascular responses")+
  xlab("Subjective reports")


plot.cor<- ggplot(data=flat_less, aes(column, row, fill=cor))+
  geom_tile(colour="white")+
  scale_fill_gradient2(low = "blue", high="red", mid = "white", midpoint = 0, limit = c(-1,1), space = "Lab", name="Pearson\nCorrelation")+
  geom_text(aes(label = sprintf("%1.2f",cor)), color = "black", size = 3.5, vjust = 0) + 
  geom_text(aes(label = p.sym), color = "black", size = 3.5, vjust = 1.5) +
  theme( axis.text.x=element_text(angle=45, vjust = 1, hjust=1, size=10),
         axis.text.y=element_text(size=10),
         axis.ticks=element_blank(),
         panel.border=element_blank(),
         panel.background=element_blank(),
         # legend.position="top",
         legend.text=element_text(size=10),
         legend.title=element_text(size=10))+
  scale_x_discrete(labels=c(expression(paste(bold(Delta), "PE")),
                            expression(paste(bold(Delta), "EX")),
                            expression(paste(bold(Delta), "CY")),
                            expression(paste(bold(Delta), "PSS")),
                            expression(paste(bold(Delta), "WEMWBS")),
                            expression(paste(bold(Delta), "log(PSQi)"))
                            ))+
  scale_y_discrete(labels=c(expression(paste(bold(Delta), HR["minHR"])),
                            expression(paste(bold(Delta), sqrt(SDNN["minHR"]))),
                            expression(paste(bold(Delta), sqrt(RMSSD["minHR"])))
                            ))+
  coord_fixed()+
  xlab("Subjective reports")+
  ylab("Cardiovascular responses")


        # +scale_x_discrete(labels=function(x) letters[1:3][which(x==levels(iris$Species))]


grid.arrange(plot.cor, nrow=1)
ggsave("sleepALL_heatmap.png", plot.cor, height=7, width = 12, units = "cm")


        # +scale_x_discrete(labels=function(x) letters[1:3][which(x==levels(iris$Species))]


# grid.arrange(plot.cor, nrow=1)
# ggsave("subjective_reports_heatmap.png", plot.cor, height=7, width = 12, units = "cm")
# 
# print(free_less)
# 
# ggsave("subjective_free_heatmap.eps", free_less, height=12, width = 18.5, units = "cm")

```

\newpage

```{r echo=FALSE, warning=FALSE, message=FALSE, fig.height=15, fig.width=13}
# idx<- which(!(flat_less$p.sym %in% ""))
# plt<- list()
# for (i in 1:length(idx)){
#   plt[[i]]<- ggplot(dfALL, aes_string(x=flat_less$row[i], y=flat_less$column[i], color=dfALL$grp))+
#     geom_point(size=3)+
#     geom_jitter()+theme(legend.position="top")+
#     xlab(expression(paste(Delta,flat_less$column[i])))+
#     ylab(expression(paste(Delta,flat_less$row[i])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )
# }
cat("Scatterplots")
HR.PE<-ggplot(dfALL, aes(x=DPE, y=DHRtot, color=grp))+
  geom_point(size=3)+
  geom_jitter()+theme(legend.position="top")+
  xlab(expression(paste(Delta,PE)))+
  ylab(expression(paste(Delta,HR["minHR"])))+
  theme(
    panel.background=element_blank(),
    panel.border = element_blank(), 
    panel.grid.minor.x=element_blank(),
    panel.grid.major.x=element_blank(),
    panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
    panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
    legend.position="top",
    legend.key = element_rect(fill = "white"), 
    axis.line = element_line(colour = "black", size=0.5)
    )

HR.EX<-ggplot(dfALL, aes(x=DEX, y=DHRtot, color=grp))+
  geom_point(size=3)+
  geom_jitter()+theme(legend.position="top")+
  xlab(expression(paste(Delta,EX)))+
  ylab(expression(paste(Delta,HR["minHR"])))+
  theme(
    panel.background=element_blank(),
    panel.border = element_blank(), 
    panel.grid.minor.x=element_blank(),
    panel.grid.major.x=element_blank(),
    panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
    panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
    legend.position="top",
    legend.key = element_rect(fill = "white"), 
    axis.line = element_line(colour = "black", size=0.5)
    )
HR.CY<-ggplot(dfALL, aes(x=DCY, y=DHRtot, color=grp))+
  geom_point(size=3)+
  geom_jitter()+theme(legend.position="top")+
  xlab(expression(paste(Delta,CY)))+
  ylab(expression(paste(Delta,HR["minHR"])))+
  theme(
    panel.background=element_blank(),
    panel.border = element_blank(), 
    panel.grid.minor.x=element_blank(),
    panel.grid.major.x=element_blank(),
    panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
    panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
    legend.position="top",
    legend.key = element_rect(fill = "white"), 
    axis.line = element_line(colour = "black", size=0.5)
    )

HR.PSS<- ggplot(dfALL, aes(x=DPSSnew, y=DHRtot, color=grp))+
  geom_point(size=3)+
  geom_jitter()+theme(legend.position="top")+
  xlab(expression(paste(Delta,PSS)))+
  ylab(expression(paste(Delta,HR["150min"])))+
  theme(
    panel.background=element_blank(),
    panel.border = element_blank(), 
    panel.grid.minor.x=element_blank(),
    panel.grid.major.x=element_blank(),
    panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
    panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
    legend.position="top",
    legend.key = element_rect(fill = "white"), 
    axis.line = element_line(colour = "black", size=0.5)
    )

HR.WEMWBS<- ggplot(dfALL, aes(x=DWEMWBSnew, y=DHRtot, color=grp))+
  geom_point(size=3)+
  geom_jitter()+theme(legend.position="top")+
  xlab(expression(paste(Delta,WEMWBS)))+
  ylab(expression(paste(Delta,HR["150min"])))+
  theme(
    panel.background=element_blank(),
    panel.border = element_blank(), 
    panel.grid.minor.x=element_blank(),
    panel.grid.major.x=element_blank(),
    panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
    panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
    legend.position="top",
    legend.key = element_rect(fill = "white"), 
    axis.line = element_line(colour = "black", size=0.5)
    )
# SDNN.EX<- ggplot(dfALL, aes(x=DEX, y=Dsq.SDNN, color=grp))+
#   geom_point(size=3)+
#   geom_jitter()+theme(legend.position="top")+
#   xlab(expression(paste(Delta,EX)))+
#   ylab(expression(paste(Delta,sq.SDNN["150min"])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )
# SDNN.CY<- ggplot(dfALL, aes(x=DCY, y=Dsq.SDNN, color=grp))+
#   geom_point(size=3)+
#   geom_jitter()+theme(legend.position="top")+
#   xlab(expression(paste(Delta,DEX)))+
#   ylab(expression(paste(Delta,sq.SDNN["150min"])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )
# SDNN.PSQI<- ggplot(dfALL, aes(x=Dlg.PSQI, y=Dsq.SDNN, color=grp))+
#   geom_point(size=3)+
#   geom_jitter()+theme(legend.position="top")+
#   xlab(expression(paste(Delta,lg.PSQI)))+
#   ylab(expression(paste(Delta,sq.SDNN["150min"])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )
# RMSSD.EX<- ggplot(dfALL, aes(x=DEX, y=Dsq.RMSSD, color=grp))+
#   geom_point(size=3)+
#   geom_jitter()+theme(legend.position="top")+
#   xlab(expression(paste(Delta,EX)))+
#   ylab(expression(paste(Delta,sq.RMSSD["150min"])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )
# RMSSD.CY<- ggplot(dfALL, aes(x=DCY, y=Dsq.RMSSD, color=grp))+
#   geom_point(size=3)+
#   geom_jitter()+theme(legend.position="top")+
#   xlab(expression(paste(Delta,CY)))+
#   ylab(expression(paste(Delta,sq.RMSSD["150min"])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )
# RMSSD.WEMWBS<- ggplot(dfALL, aes(x=DWEMWBSnew, y=Dsq.RMSSD, color=grp))+
#   geom_point(size=3)+
#   geom_jitter()+theme(legend.position="top")+
#   xlab(expression(paste(Delta,WEMWBS)))+
#   ylab(expression(paste(Delta,sq.RMSSD["150min"])))+
#   theme(
#     panel.background=element_blank(),
#     panel.border = element_blank(), 
#     panel.grid.minor.x=element_blank(),
#     panel.grid.major.x=element_blank(),
#     panel.grid.minor.y = element_line(colour = "#d9d9d9",  size=0.2), 
#     panel.grid.major.y = element_line(colour = "#bfbfbf", size=0.4),  
#     legend.position="top",
#     legend.key = element_rect(fill = "white"), 
#     axis.line = element_line(colour = "black", size=0.5)
#     )

sleepALL_scatter_s<- arrangeGrob(HR.PE,HR.EX, HR.CY, nrow=2)
# sleep150_scatter_ns<- arrangeGrob(SDNN.CY, SDNN.PSQI, RMSSD.CY, RMSSD.WEMWBS)

ggsave("subjective_sleepALL_scatter_s.png", sleepALL_scatter_s, height=12, width = 12, units = "cm")
  
```
